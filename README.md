# doctrine/dbal

Database Abstraction Layer https://packagist.org/packages/doctrine/dbal

[![PHPPackages Rank](http://phppackages.org/p/doctrine/dbal/badge/rank.svg)](http://phppackages.org/p/doctrine/dbal)
[![PHPPackages Referenced By](http://phppackages.org/p/doctrine/dbal/badge/referenced-by.svg)](http://phppackages.org/p/doctrine/dbal)

[TOC]

# [Official documentation](http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/)
(Use corresponding often current version)
* https://www.doctrine-project.org/projects/dbal.html
* [*Getting a Connection*](https://www.doctrine-project.org/projects/doctrine-dbal/en/2.10/reference/configuration.html)

## Data Definition Language (DDL)
* [*Schema-Representation*
  ](https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/schema-representation.html)
* [*Schema-Manager*
  ](https://www.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/schema-manager.html)

# Fluid schema builder
* [![PHPPackages Rank](http://phppackages.org/p/thecodingmachine/dbal-fluid-schema-builder/badge/rank.svg)](http://phppackages.org/p/thecodingmachine/dbal-fluid-schema-builder)
  [![PHPPackages Referenced By](http://phppackages.org/p/thecodingmachine/dbal-fluid-schema-builder/badge/referenced-by.svg)](http://phppackages.org/p/thecodingmachine/dbal-fluid-schema-builder)
  thecodingmachine/[dbal-fluid-schema-builder](https://packagist.org/packages/thecodingmachine/dbal-fluid-schema-builder)
* [*Fluent Interfaces are Evil*
  ](http://ocramius.github.io/blog/fluent-interfaces-are-evil/)
  [2013-11](https://github.com/Ocramius/ocramius.github.com/blob/source/source/_posts/2013-11-07-fluent-interfaces-are-evil.md)
  Marco Pivetta (may be a major contributor to Doctrine)
  More about fluent (fluid?) interfaces at
  [*Notes on computer programming languages*
  ](https://gitlab.com/notes-on-computer-programming-languages/notes-on-computer-programming-languages)

# Written about
* [*Use PHP Enums as Doctrine type in Symfony*
  ](https://smaine-milianni.medium.com/use-php-enums-as-doctrine-type-in-symfony-85909aa0a19a)
  2021-12 Smaine Milianni
* [*Split PDO and DBAL adapters*
  ](https://github.com/symfony/symfony/issues/42962)
  2021-09 derrabus
* [*Stocker des dates avec leur timezone, avec Doctrine*
  ](https://blog.netinfluence.ch/2019/03/21/stocker-des-dates-avec-leur-timezone-avec-doctrine/)
  2019-03 [Romaric](https://blog.netinfluence.ch/author/romaric/)
* [*Doctrine and Generated Columns*
  ](https://www.liip.ch/en/blog/doctrine-and-generated-columns)
  2019-02 David Buchmann

# TODO
* Demonstrate DB connection and basic opertions on it like list DB
